const router = require("express").Router();
const { index, showAddImage, saveImage, deleteImage } = require("../controllers/index.ctrl");

router.get("/", index);
router.get("/images/add", showAddImage);

router.post("/images/add", saveImage);
router.get("/images/delete/:photo_id", deleteImage);

module.exports = router;
