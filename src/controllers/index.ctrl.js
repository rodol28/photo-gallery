const Photo = require("../models/Photo");
const cloudinary = require("cloudinary").v2;

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
});

const fs = require("fs-extra");

const control = {};

control.index = async (req, res) => {
  const photos = await Photo.find().lean();
  console.log(photos);
  res.render("images", { photos });
};

control.showAddImage = async (req, res) => {
  const photos = await Photo.find().lean();
  res.render("images_form", { photos });
};

control.saveImage = async (req, res) => {
  const { title, description } = req.body;

  const result = await cloudinary.uploader.upload(req.file.path);

  const newPhoto = new Photo({
    title,
    description,
    imageURL: result.secure_url,
    public_id: result.public_id,
  });

  await newPhoto.save();

  await fs.unlink(req.file.path);

  res.redirect("/");
};

control.deleteImage = async (req, res) => {
  const { photo_id } = req.params;
  const photo = await Photo.findByIdAndDelete(photo_id);
  const result = await cloudinary.uploader.destroy(photo.public_id);
  console.log(result);
  res.redirect('/images/add');
};

module.exports = control;
