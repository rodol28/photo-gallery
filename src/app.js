const express = require("express");
const morgan = require("morgan");
const multer = require("multer");
const path = require("path");
const exphbs = require("express-handlebars");

// initializations
const app = express();
require('./database');

// settings
app.set("port", process.env.PORT || 3002);
app.set("views", path.join(__dirname, 'views'));

app.engine(".hbs", exphbs({
  defaultLayout: 'main',
  layoutsDir: path.join(app.get('views'), 'layouts'),
  partialsDir: path.join(app.get('views'), 'partials'),
  extname: '.hbs'
}));
app.set('view engine', '.hbs');

// middlewares
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}
/* Permite que express puede entender datos de tipo json 
que se le envien */
app.use(express.json());
/* Permite que express puede entender datos que es envien 
desde un formulario. En este caso solo datos (strings), no archivos pesados, 
ya que las imagenes las manejaremos a traves de multer*/
app.use(express.urlencoded({extended: false}));

/* Se configuran la ubicacion donde se subiran las imagenes y 
su nombre aleatorio en funcion de Date.now() + su extension */
const storage = multer.diskStorage({
  destination: path.join(__dirname, 'public/uploads'),
  filename: (req, file, cb) => {
    cb(null, Date.now() + path.extname(file.originalname))
  }
});
/* Observa si cada vez que enviamos datos al servidor 
estamos enviando una imagen, si enviamos una imagen el
va a poder interpretar esa imagen y colocarla dentro de
nuestro servidor.
Debemos decirle a traves de .single() que campo debe observar
del formulario. En este caso el formulario tendra un campo 
llamado'image' el cual usaremos para subir a imagen.
Con el objeto storage se configura el lugar donde se guardaran
imagenes dentro de nuestro servidor y el nombre aleatorio que 
recibira cada imagen subida */
app.use(multer({storage}).single('image'));

// routes
app.use(require('./routes'));

module.exports = app;